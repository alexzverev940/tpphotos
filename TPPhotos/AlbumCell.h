//
//  AlbumCell.h
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionData.h"

@interface AlbumCell : UITableViewCell

- (void)configureWithAlbumModel:(AlbumModel *)model assetIndex:(NSUInteger)index;
@end
