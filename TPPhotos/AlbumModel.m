//
//  AlbumModel.m
//  TPPhotos
//
//  Created by Alex Zverev on 01.05.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "AlbumModel.h"

@implementation AlbumModel

- (void)loadAssetAtIndex:(NSUInteger)index ofSize:(CGSize)imageViewSize withComplete:(void (^)(UIImage *assetImage, NSString* assetId))onCompletion {
    CGSize targetSize = CGSizeMake(imageViewSize.width*[UIScreen mainScreen].scale, imageViewSize.height*[UIScreen mainScreen].scale);
    PHAsset *asset = [self.fetchResult objectAtIndex:index];
    PHImageRequestOptions * imageRequestOptions = [PHImageRequestOptions new];
    imageRequestOptions.synchronous = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:targetSize
                                                  contentMode:PHImageContentModeAspectFit
                                                      options:imageRequestOptions
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                    onCompletion(result, asset.localIdentifier);
                                                }];
    });
}

@end
