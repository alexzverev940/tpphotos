//
//  AlbumItemCell.h
//  TPPhotos
//
//  Created by Alex Zverev on 05.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumModel.h"

@interface AlbumItemCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;

- (void)configureWithAlbumModel:(AlbumModel *)model assetIndex:(NSUInteger)index andImageViewSize:(CGSize)imageViewSize;
@end
