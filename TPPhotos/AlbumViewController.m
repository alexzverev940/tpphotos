//
//  AlbumViewController.m
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "AlbumViewController.h"
#import "AlbumItemCell.h"
#import "PreviewPhotoViewController.h"

static NSString * const PreviewPhotoCellReuseId = @"PreviewPhotoCellID";

@interface AlbumViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation AlbumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.navigationItem.title = self.albumModel.title;
}

#pragma mark - COLLECTION VIEW DELEGATE
//size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake([AlbumViewController cellSideSize], [AlbumViewController cellSideSize]);
}

//count
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.albumModel.fetchResult.count;
}

//cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AlbumItemCell *cell = (AlbumItemCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:PreviewPhotoCellReuseId forIndexPath:indexPath];
    CGSize targetSize = CGSizeMake([AlbumViewController cellSideSize], [AlbumViewController cellSideSize]);
    [cell configureWithAlbumModel:self.albumModel assetIndex:indexPath.row andImageViewSize:targetSize];

    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PreviewPhotoViewController* vc = [segue destinationViewController];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    vc.asset = [self.albumModel.fetchResult objectAtIndex:indexPath.row];
}

#pragma mark - Helper methods

+ (CGFloat)cellSideSize {
    return ([UIScreen mainScreen].bounds.size.width - 20)/3.0;
}

@end
