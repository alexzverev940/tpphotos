//
//  PreviewPhotoViewController.m
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "PreviewPhotoViewController.h"

@interface PreviewPhotoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation PreviewPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadImage];
}

- (void)loadImage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CGSize targetSize = CGSizeMake(self.imageView.bounds.size.width * [UIScreen mainScreen].scale,
                                       self.imageView.bounds.size.height * [UIScreen mainScreen].scale);
        [[PHImageManager defaultManager] requestImageForAsset:self.asset targetSize:targetSize contentMode:PHImageContentModeAspectFit options:nil resultHandler:^(UIImage *result, NSDictionary *info) {
            if (result) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.imageView.image = result;
                });
            }
        }];
    });
}

@end
