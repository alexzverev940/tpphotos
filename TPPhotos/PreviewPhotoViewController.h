//
//  PreviewPhotoViewController.h
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

@interface PreviewPhotoViewController : UIViewController
@property (strong, nonatomic) PHAsset* asset;
@end
