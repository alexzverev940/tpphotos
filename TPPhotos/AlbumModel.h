//
//  AlbumModel.h
//  TPPhotos
//
//  Created by Alex Zverev on 01.05.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

@interface AlbumModel : NSObject
@property (nonatomic, strong)NSString *title;
@property (nonatomic, strong)NSString *countOfImages;
@property (nonatomic, strong)PHFetchResult<PHAsset *> *fetchResult;
//@property (nonatomic, strong) UIImage* cover;
- (void)loadAssetAtIndex:(NSUInteger)index ofSize:(CGSize)imageViewSize withComplete:(void (^)(UIImage *assetImage, NSString* assetId))onCompletion;
@end
