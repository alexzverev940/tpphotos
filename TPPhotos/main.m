//
//  main.m
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
