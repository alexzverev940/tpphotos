//
//  CollectionsData.m
//  TPPhotos
//
//  Created by Alex Zverev on 01.05.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "CollectionData.h"

@interface CollectionData()
@property (strong, nonatomic) NSMutableArray *collectionsFetchResults;//PHFetchResult<PHAsset *>*
@property (strong, nonatomic) NSMutableArray *assetCollections;//PHAssetCollection*
@end

@implementation CollectionData

-(instancetype)init {
    self = [super init];
    if (self) {
//        [self checkAccessToPhotoLibrary];
    }
    return self;
}

- (NSMutableArray *)assetCollections {
    if (!_assetCollections) {
        _assetCollections = [NSMutableArray new];
    }
    return _assetCollections;
}
- (NSMutableArray *)collectionsFetchResults {
    if (!_collectionsFetchResults) {
        _collectionsFetchResults = [NSMutableArray new];
    }
    return _collectionsFetchResults;
}


- (void)checkAccessToPhotoLibraryWithSuccess:(void (^)())onSuccess error:(void (^)())onError {
    if ([PHPhotoLibrary authorizationStatus]!=PHAuthorizationStatusAuthorized) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            NSLog(@"%ld", (long)status);
            if (status != PHAuthorizationStatusAuthorized) {
                onError();
            } else {
                onSuccess();
            }
        }];
    } else {
        onSuccess();
    }
}

- (void)loadData {
    //
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    PHFetchResult *userAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    
    
    void (^assetsEnumeratorBlock)(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) = ^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        
        PHFetchOptions *filterOptions = [PHFetchOptions new];
        filterOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %i", PHAssetMediaTypeImage];
        filterOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
        PHFetchResult<PHAsset *> *result = [PHAsset fetchAssetsInAssetCollection:collection options:filterOptions];
        if (result.count > 0) {
            [self.collectionsFetchResults addObject:result];
            [self.assetCollections addObject:collection];
//            NSLog(@"== %@ \t(%lu)", collection.localizedTitle, (unsigned long)result.count);
        }
    };
    
    [smartAlbums enumerateObjectsUsingBlock:assetsEnumeratorBlock];
    [userAlbums enumerateObjectsUsingBlock:assetsEnumeratorBlock];
//    NSLog(@"== all Enumerated");
}

- (NSInteger)countOfAlbums {
    return self.assetCollections.count;
}

- (AlbumModel *)getAlbumDataAdIndex:(NSUInteger)index {
    PHFetchResult<PHAsset *> *assetsFetchResult = self.collectionsFetchResults[index];

    AlbumModel* album = [AlbumModel new];
    album.title = ((PHAssetCollection *)self.assetCollections[index]).localizedTitle;
    album.countOfImages = [NSString stringWithFormat: @"%lu images", (unsigned long)assetsFetchResult.count];
    album.fetchResult = assetsFetchResult;
    return album;
}

@end
