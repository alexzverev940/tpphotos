//
//  AlbumCell.m
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "AlbumCell.h"
#import "CollectionData.h"

@interface AlbumCell()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (nonatomic, copy) NSString *assetId;
@end

@implementation AlbumCell

- (instancetype)init {
    self = [super init];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configureWithAlbumModel:(AlbumModel *)model assetIndex:(NSUInteger)index {
    self.titleLabel.text = model.title;
    self.countLabel.text = model.countOfImages;
    self.assetId = model.fetchResult[index].localIdentifier;
    [model loadAssetAtIndex:index ofSize:self.coverImageVIew.frame.size withComplete:^(UIImage *assetImage, NSString* assetId) {
        if ([self.assetId isEqualToString:assetId]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.coverImageVIew setImage:assetImage];
            });
        }
    }];
}

@end
