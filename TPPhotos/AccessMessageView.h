//
//  AccessMessageView.h
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AccessMessageViewDelegate <NSObject>
//- (void) didTapAllowAccess;
@end

typedef NS_ENUM(NSInteger, ViewTag) {
    kAccessMessageTag = 1135,
};

@interface AccessMessageView : UIView
//-(id) initWithParentView: (UIView *)parentView;
-(void) addToParentView:(UIView *) parentView;


//@property (weak, nonatomic) id<AccessMessageViewDelegate> delegate;
@end
