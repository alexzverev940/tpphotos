//
//  AccessMessageView.m
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "AccessMessageView.h"
#import "AlbumsListViewController.h"

@implementation AccessMessageView

-(instancetype) init {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"AccessMessageView" owner:self options:nil];
    AccessMessageView *accessMessageView = [subviewArray objectAtIndex:0];
    if (accessMessageView) {
        accessMessageView.tag = kAccessMessageTag;
        accessMessageView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return accessMessageView;
}

-(void) addToParentView:(UIView *) parentView  {
    if (self) {
        [parentView addSubview: self];
        
        //add Constraints
        NSDictionary *viewsDictionary = @{@"accessMessageView":self};
        NSArray *constraintVertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[accessMessageView]-0-|"
                                                                              options:0
                                                                              metrics:nil
                                                                                views:viewsDictionary];
        
        NSArray *constraintHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[accessMessageView]-0-|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary];
        [parentView addConstraints:constraintVertical];
        [parentView addConstraints:constraintHorizontal];
    }
}


@end
