//
//  ViewController.m
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "AlbumsListViewController.h"
#import "AccessMessageView.h"
#import "AlbumCell.h"
#import "AlbumViewController.h"
#import "CollectionData.h"

@interface AlbumsListViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) CollectionData* collectionData;
@end

@implementation AlbumsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionData = [CollectionData new];
    [self.collectionData checkAccessToPhotoLibraryWithSuccess:^{
        [self loadAlbums];
    } error:^{
        [self showAccessWarningMessage];
    }];
}

#pragma mark - access

- (void)showAccessWarningMessage {
    if (![self.view viewWithTag:kAccessMessageTag]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            AccessMessageView *accessMessageView = [AccessMessageView new];
            [accessMessageView addToParentView:self.view];
        });
    }
}

#pragma mark - Loas
- (void)loadAlbums {
    [self.collectionData loadData];
    [self.tableView reloadData];
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;//TODO: fix this magic
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.collectionData countOfAlbums];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AlbumCell class]) forIndexPath:indexPath];
    AlbumModel* albumModel = [self.collectionData getAlbumDataAdIndex:indexPath.row];
    NSUInteger indexOfLastAssetForPreview = albumModel.fetchResult.count-1;
    [cell configureWithAlbumModel:albumModel assetIndex:indexOfLastAssetForPreview];
    
    return cell;
}

#pragma mark - SEGUE
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AlbumViewController* albumVC = [segue destinationViewController];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    albumVC.albumModel = [self.collectionData getAlbumDataAdIndex:indexPath.row];
}

@end
