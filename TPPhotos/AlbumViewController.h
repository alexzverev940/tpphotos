//
//  AlbumViewController.h
//  TPPhotos
//
//  Created by Alex Zverev on 03.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumModel.h"
#import <Photos/Photos.h>

@interface AlbumViewController : UIViewController
@property (strong, nonatomic) AlbumModel *albumModel;
@end
