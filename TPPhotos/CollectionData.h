//
//  CollectionsData.h
//  TPPhotos
//
//  Created by Alex Zverev on 01.05.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import "AlbumModel.h"



@interface CollectionData : NSObject


- (void)checkAccessToPhotoLibraryWithSuccess:(void (^)())onSuccess error:(void (^)())onError;
- (void)loadData;
- (NSInteger)countOfAlbums;
- (AlbumModel *)getAlbumDataAdIndex:(NSUInteger)index;

@end
