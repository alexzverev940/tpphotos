//
//  AlbumItemCell.m
//  TPPhotos
//
//  Created by Alex Zverev on 05.04.16.
//  Copyright © 2016 argas. All rights reserved.
//

#import "AlbumItemCell.h"

@interface AlbumItemCell()
@property (nonatomic, copy) NSString *assetId;
@end

@implementation AlbumItemCell
- (void)configureWithAlbumModel:(AlbumModel *)model assetIndex:(NSUInteger)index andImageViewSize:(CGSize)imageViewSize {
    self.assetId = model.fetchResult[index].localIdentifier;
    [model loadAssetAtIndex:index ofSize:imageViewSize withComplete:^(UIImage *assetImage, NSString* assetId) {
        if ([self.assetId isEqualToString:assetId]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.imagePreview setImage:assetImage];
            });
        }
    }];
}

@end
